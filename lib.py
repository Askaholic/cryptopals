import string
import struct


WEIGHTS = {
    "lowercase": 4,
    "uppercase": 3,
    "numbers": 2,
    "other": 1,
    "non-ascii": -5
}


def xor(s1, s2):
    assert len(s1) == len(s2)
    result = bytearray()
    for c1, c2 in zip(s1, s2):
        result += bytes([c1 ^ c2])
    return bytes(result)


def xor_key(s1, key):
    l = len(key)
    s2 = key * (len(s1) // l + 1)
    return xor(s1, s2[:len(s1)])


def ascii_freq(string_input):
    score = 0
    for char in string_input:
        try:
            char = chr(char)
        except Exception:
            pass
        score += get_char_likelyhood_score(char)
    return score


def get_char_likelyhood_score(char):
    if char in string.ascii_lowercase:
        return WEIGHTS['lowercase']
    if char in string.ascii_uppercase or char == " ":
        return WEIGHTS['uppercase']
    if char in string.digits or char == "_":
        return WEIGHTS['numbers']
    if char in string.printable:
        return WEIGHTS['other']
    return WEIGHTS['non-ascii']


def ascii_freq_hist(string_input):
    hist = [0] * 127
    for char in string_input:
        if chr(char) in string.printable:
            hist[char] += 1
    return hist


def ascii_freq_hist_likelyhood_score(hist):
    score = 0
    for i, num in enumerate(hist):
        char = chr(i)
        score += get_char_likelyhood_score(char) * num
    return score


def hamming_dist(s1, s2):
    assert len(s1) == len(s2)

    dist = 0
    for c1, c2 in zip(s1, s2):
        bits1 = bin(c1)[2:]
        bits2 = bin(c2)[2:]

        bits1 = "0" * (8 - len(bits1)) + bits1
        bits2 = "0" * (8 - len(bits2)) + bits2

        for b1, b2 in zip(bits1, bits2):
            if b1 != b2:
                dist += 1

    return dist


def print_hist(hist, divisor=1):
    for i, num in enumerate(hist):
        if num > 0:
            print(chr(i), ":", "*" * (num // divisor))


def blocks_of(s, size=16):
    for i in range(0, len(s), size):
        yield s[i: i + size]


def pkcs7_pad(s, size=16):
    assert size < 256
    padding_amount = size - (len(s) % size)
    return s + bytes([padding_amount]) * padding_amount


def pkcs7_unpad(s):
    padding_amount = s[-1]
    return s[:-padding_amount]
