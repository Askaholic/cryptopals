import os
import re
from base64 import b64decode
from collections import OrderedDict
from random import randint

from Crypto.Cipher import AES
from lib import blocks_of, pkcs7_pad, pkcs7_unpad, xor

ECB_KEY = os.urandom(16)


def chal9():
    print(pkcs7_pad(b'YELLOW SUBMARINE', size=20))


def chal10():
    cipher = AES.new('YELLOW SUBMARINE')
    data = None
    with open('10.txt', 'r') as f:
        data = b64decode(f.read().replace('\n', ''))

    print(cbc_decrypt(cipher, data))


def cbc_encrypt(ecb_cipher, data, iv=b'\x00' * 16):
    prev_block = iv
    result = bytearray()
    for block in blocks_of(pkcs7_pad(data)):
        enc_block = ecb_cipher.encrypt(xor(block, prev_block))
        prev_block = enc_block
        result += enc_block
    return bytes(result)


def cbc_decrypt(ecb_cipher, data, iv=b'\x00' * 16):
    prev_block = iv
    result = bytearray()
    for block in blocks_of(data):
        decrypted = ecb_cipher.decrypt(block)
        result += xor(prev_block, decrypted)
        prev_block = block

    return bytes(result)


def chal11():
    encrypted = encryption_oracle(b"a" * 100)
    print(encrypted)

    print()
    print("Xoring blocks")
    prev_block = None
    is_ecb = False
    for block in blocks_of(encrypted):
        if prev_block:
            xored = xor(block, prev_block)
            print(xored)
            if xored == b"\x00" * 16:
                is_ecb = True
        prev_block = block

    print()
    print("{} detected".format("ECB" if is_ecb else "CBC "))


def encryption_oracle(data):
    key = os.urandom(16)
    mode = AES.MODE_CBC if os.urandom(1)[0] > 127 else AES.MODE_ECB
    data = os.urandom(randint(5, 10)) + data + os.urandom(randint(5, 10))

    cipher = AES.new(key)
    if mode == AES.MODE_ECB:
        return cipher.encrypt(pkcs7_pad(data))

    return cbc_encrypt(cipher, data, iv=os.urandom(16))


def chal12():
    # Find block size, we know it's 16, but this would discover any block
    # size up to max size
    block_size = find_block_size(16)

    # Verify ECB is being used
    enc = ecb_function(b"A" * (block_size * 2))
    assert enc[:block_size] == enc[block_size:block_size*2]

    # Decode the message
    known = bytearray()
    block_num = 0
    while True:
        try:
            for l in range(0, block_size):
                prefix = b"A" * (block_size - (l + 1))
                lookup = make_lookup_table(prefix, known, block_size, block_index=block_num)

                enc = ecb_function(prefix)
                known += lookup[enc[block_size * block_num:block_size + block_size * block_num]]
        except KeyError:
            break
        block_num += 1

    # Done
    print(bytes(known).decode())


def make_lookup_table(prefix, known, block_size, block_index=0):
    lookup = {}
    for i in range(256):
        byt = bytes([i])
        enc = ecb_function(prefix + known + byt)
        lookup[enc[block_size * block_index:block_size + block_size*block_index]] = byt
    return lookup


def find_block_size(max_size):
    prev_len = None
    for i in range(1, max_size):
        length = len(ecb_function(b'A' * i * 2))
        if prev_len and prev_len != length:
            return length - prev_len
        prev_len = length


def ecb_function(data):
    ''' Encrypts data plus a secret using ECB and a consistent key '''
    secret = ("Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg"
              "aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq"
              "dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg"
              "YnkK")
    secret = b64decode(secret)
    data += secret

    cipher = AES.new(ECB_KEY)

    return cipher.encrypt(pkcs7_pad(data))


def chal13():
    enc_profile = profile_for("asd@gmail.com")
    emailbytes = enc_profile[:32]

    enc_profile = profile_for(b"a" * 10 + b"admin" + (b'\x0b' * 0xb))
    rolebytes = enc_profile[16:32]

    print(decrypt_profile(emailbytes + rolebytes))


def profile_for(email):
    # This regex actually makes this problem a lot harder... Even impossibe?
    # m = re.match(r'^[a-zA-Z\.]+@[a-zA-Z\.]+\.(com|org|gov)$', email)
    # if m is None:
    #     raise Exception("Invalid email")
    if isinstance(email, str):
        email = email.encode()

    email = email.replace(b'&', b'').replace(b'=', b'')
    profile = b"email=" + email + b"&uid=10&role=user"

    cipher = AES.new(ECB_KEY)
    return cipher.encrypt(pkcs7_pad(profile))


def decrypt_profile(encrypted):
    cipher = AES.new(ECB_KEY)
    return pkcs7_unpad(cipher.decrypt(encrypted))


def url_style_parse(input_string):
    elements = input_string.split('&')
    result = {}
    for el in elements:
        m = re.match(r'(.*)=(.*)', el)
        if m is None:
            continue
        (key, value) = m.groups()
        result[key] = value
    return result


if __name__ == '__main__':
    chal13()
